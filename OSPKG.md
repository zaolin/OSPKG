# OSPKG



## Data Format

![](OSPKG_file.png)

### Header

The header is in total 17 bytes long and consists just of the necessary data.

| Label           | Size   | Usage                         |
| --------------- | ------ | ----------------------------- |
| Magic           | uint64 | OSPKG format identifier       |
| Version         | uint8  | OSPKG format version          |
| SignatureOffset | uint64 | OSPKG format signature offset |

### Artifact

Each artifact is a key/value pair containing a name/identifier and the raw data.

|        |        |                                   |
| ------ | ------ | --------------------------------- |
| Name   | uint64 | Identifier for artifacts contents |
| Length | uint32 | Length of Data in bytes           |
| Data   | []byte | Data matching the identifier      |

### Payload

The payload consists of a maximum of 256 artifacts.

| Label   | Size        | Usage               |
| ------- | ----------- | ------------------- |
| Length  | uint8       | Number of artifacts |
| Content | []Artifacts | Array of artifacts  |

### Signature

The OSPKG signature uses PureEdDSA also known as Ed25519ph. The hash function can be choosen.

| Label     | Size   | Usage                              |
| --------- | ------ | ---------------------------------- |
| Hash      | uint8  | Type of hash function used for PH  |
| Length    | uint32 | Length of SectionData in bytes     |
| Signature | []byte | Signature of 0...SignatureOffset-1 |

### Default values

| Data Type        | Value/s                      | Description                                    |
| ---------------- | ---------------------------- | ---------------------------------------------- |
| Magic (Header)   | OSPKG\n\n\n                  | File magic number                              |
| Hash (Signature) | 0x0 = SHAKE512, 0x1 = SHA512 | Type of hash used in signature operation as PH |

### Type of Sections

| Name    | Description                                              |
| ------- | -------------------------------------------------------- |
| kernel  | Linux Kernel in elf/coff format, no compression          |
| initrd  | Linux Initramfs in cpio format with optional compression |
| cmdline | Linux Kernel Cmdline in text format                      |

## Signing

